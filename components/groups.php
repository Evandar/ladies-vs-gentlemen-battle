<!-- program start -->
<!-- ================ -->
<section id="groups" class="dark-bg pv-30 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="text-center"><?php echo __('Opis poziomów'); ?></h2>
                <div class="separator"></div>
                <p>
                    <?php pll_e('Grupy podczas warsztatów podzielone będą ze względu na płeć: Ladies i Gentlemen. Dodatkowo wprowadzamy podział na poziomy zaawansowania:') ?>
                </p>
                <ul>
                    <li>
                        <strong>Jitterbugs</strong> - <?php pll_e('jeśli znasz podstawy Lindy Hopa ale dopiero zaznajamiasz się z Solo Jazzem (może nawet znasz całego Shim Shama!), ta grupa będzie idealna dla Ciebie. Nauczysz się tu choreografii zawierającej ciekawe ruchy, rytmiki i konfiguracje, które odtańczone wieczorem razem z całą grupą wprawią w osłupienie przeciwnika.'); ?>
                    </li>
                    <li>
                        <strong>Hepcats</strong> - <?php pll_e('grupa dla nieco bardziej zaawansowanych zawodników; jeśli masz na swoim koncie wiele przetańczonych nocy, niejednokrotnie miałeś do czynienia z Solo Jazzem, dysponujesz szerokim wachlarzem ruchów i rytmik oraz niestraszne Ci szybsze tempo nauki - ta grupa jest dla Ciebie!'); ?>
                        <em><?php pll_e('Sugerowany poziom dla grup LH4/kursantów zajęć Solo Jazz oraz starszych stażem tancerzy.');?></em>
                    </li>
                </ul>
                <p>
                    <?php pll_e('Podsumowując: podczas zapisów wybierasz grupę oraz poziom, np. Ladies - Hepcats.') ?>
                </p>
                </p>
            </div>
        </div>
    </div>
</section><!-- section end -->