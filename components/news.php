<!-- section start -->
<!-- ================ -->
<section id="news" class="section light-translucent-bg force-dark-bg tickets-bg background-img-2">
    <div class="container">
        <div class="col-lg-8 offset-lg-2">
            <h2 style="color: #FFFFFF;" class="text-center"><?php pll_e('News'); ?></h2>
            <div class="separator"></div>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <?php $custom_query = new WP_Query('posts_per_page=6'); // exclude category 9
                while($custom_query->have_posts()) : $custom_query->the_post(); ?>
                    <?php get_template_part( 'components/single_news', get_post_format() ); ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); // reset the query ?>
            </div>
        </div>
        <a href="<?php echo get_permalink(pll_get_post(get_page_by_title('All news')->ID)); ?>"
           class="btn btn-default pull-right "><?php pll_e('All news'); ?></a>
    </div>
</section>
<!-- section end -->

