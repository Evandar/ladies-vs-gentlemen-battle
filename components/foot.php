<script>
    // Add smooth scrolling on all links inside the navbar

    function scrollToId(event) {
        console.log(this.hash)
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash) {

            // Prevent default anchor click behavior
            if (event){
                event.preventDefault();
            }

            // Store hash
            var hash = this.hash;

            if (!$(hash)[0]) {
                window.location.hash = hash;
                window.location.pathname = '/pl/';
                return
            }

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    }

    $(".navbar a").on('click', scrollToId);
    $(function (){
        scrollToId();
    });
</script>