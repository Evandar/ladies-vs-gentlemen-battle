<div class="col-lg-4">
    <div class="image-box style-2 mb-20 shadow-2 bordered light-gray-bg text-center force-dark-font">
        <div class="overlay-container">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('large'); ?>
            </a>
        </div>
        <div class="body">
            <div class="separator clearfix"></div>
            <h3><?php the_title(); ?></h3>
            <div class="separator clearfix"></div>
            <?php the_excerpt(); ?>
            <a href="<?php the_permalink(); ?>" class="btn btn-default btn-hvr hvr-shutter-out-horizontal margin-clear">
                <? pll_e('Read More'); ?><i class="fa fa-arrow-right pl-10"></i>
            </a>
        </div>
    </div>
</div>