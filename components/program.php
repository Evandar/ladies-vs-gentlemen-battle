<!-- program start -->
<!-- ================ -->
<section class="dark-bg" id="program">
    <div class="container">
            <div class="row mb-20">
                <!-- main start -->
                <!-- ================ -->
                <div class="main col-md-12">
                    <!-- page-title start -->
                    <!-- ================ -->
                    <h2 class="mt-4 text-center"><?php pll_e('Program'); ?></h2>
                    <div class="separator"></div>
                    <!-- page-title end -->
                </div>
                <!-- main end -->
            </div>
            <div class="row">

                <div class="col-lg-6 offset-lg-3 mb-20">
                    <table class="table table-schedule table-bordered">
                        <tr class="text-center -lighter"><td colspan="3"><?php pll_e('Piątek'); ?></td></tr>
                        <tr class="text-center -lighter">
                            <td></td>
                            <td>Ladies</td>
                            <td>Gentlemen</td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">18:30 - 20:30</td>
                            <td>
                                <strong>JITTERBUGS</strong>
                                <p>
                                    <?php pll_e('Przygotowania do bitwy'); ?> (Kasia)<br>
                                    <em>Akademia Artystyczna</em>
                                </p>
                            </td>
                            <td rowspan="2">
                                <strong>JITTERBUGS</strong>
                                <p>
                                    <?php pll_e('Przygotowania do bitwy'); ?><br>
                                    <em>Akademia Artystyczna</em>
                                </p>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">18:30 - 21:30</td>
                            <td>
                            </td>

                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">22:00 - 2:00</td>
                            <td colspan="2">
                                <strong>Impreza Swingowa</strong>
                                <p>
                                    <em>Gedanus</em>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-lg-6 offset-lg-3 mb-20">
                    <table class="table table-schedule table-bordered">
                        <tr class="text-center -lighter"><td colspan="3"><?php pll_e('Sobota'); ?></td></tr>
                        <tr class="text-center -lighter">
                            <td></td>
                            <td>Ladies</td>
                            <td>Gentlemen</td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">10:00 - 13:00</td>
                            <td>
                                <strong>HEPCATS</strong><br>
                                <p>
                                    <?php pll_e('Przygotowania do bitwy'); ?><br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                            <td>
                                <strong>HEPCATS</strong><br>
                                <p>
                                    <?php pll_e('Przygotowania do bitwy'); ?><br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">13:30 - 15:30</td>
                            <td>
                                <strong>JITTERBUGS + HEPCATS</strong>
                                <p>
                                    <?php pll_e('Przygotowania do bitwy'); ?><br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                            <td>
                                <strong>JITTERBUGS + HEPCATS</strong>
                                <p>
                                    <?php pll_e('Przygotowania do bitwy'); ?><br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">19:00 - 21:00</td>
                            <td>
                                <strong>JITTERBUGS</strong>
                                <p>
                                    <?php pll_e('Przygotowania do bitwy'); ?> (Vassia)<br>
                                    <em>Akademia Artystyczna</em>
                                </p>
                            </td>
                            <td>

                            </td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">21:00 - 02:00</td>
                            <td colspan="2">
                                <strong><?php pll_e('Impreza swingowa z bitwą Ladies vs Gentlemen'); ?></strong>
                                <p>
                                    <em>Gedanus</em>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-lg-6 offset-lg-3">
                    <table class="table table-schedule table-bordered mb-20">
                        <tr class="text-center -lighter"><td colspan="3"><?php pll_e('Niedziela'); ?></td></tr>
                        <tr class="text-center -lighter">
                            <td></td>
                            <td>Ladies</td>
                            <td>Gentlemen</td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">10:00 - 12:00</td>
                            <td>
                                <strong>JITTERBUGS</strong>
                                <p>
                                    Followers' track<br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                            <td>
                                <strong>JITTERBUGS</strong><br>
                                <p>
                                    Leaders' track<br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">12:00 - 12:30</td>
                            <td colspan="2">
                                <strong>JITTERBUGS</strong>
                                <p>
                                    <?php pll_e('Wspólny practice'); ?><br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">14:00 - 16:00</td>
                            <td>
                                <strong>HEPCATS</strong><br>
                                <p>
                                    Followers' track<br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                            <td>
                                <strong>HEPCATS</strong><br>
                                <p>
                                    Leaders' track<br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td class="-lighter">16:00 - 16:30</td>
                            <td colspan="2">
                                <strong>HEPCATS</strong><br>
                                <p>
                                    <?php pll_e('Wspólny practice'); ?><br>
                                    <em>Dance Atelier</em>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="row mt-20">
                <div class="col-lg-6 offset-lg-3 text-center">
                    <p>Akademia Artystyczna - ul. Aleja Grunwaldzka 339, Gdańsk</p>
                    <p>Dance Atelier - ul. Aleja Grunwaldzka 301, Gdańsk</p>
                    <p>Gedanus - ul. św. Barbary 3, Gdańsk</p>
                </div>
            </div>

        </div>
    </div>
</section><!-- section end -->