<!-- section start -->
<!-- ================ -->
<section id="about" class="light-gray-bg pv-30 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="text-center"><?php echo __('Ladies vs Gentlemen Battle 2019'); ?></h2>
                <div class="separator"></div>
                <p>
                    <?php pll_e('W jednym narożniku grupa zaciekłych w boju mężczyzn, w drugim kobiety, które nie uznają kompromisów... i tylko jedna piosenka, żeby wywalczyć zwycięstwo.') ?>
                </p>
                <p>
                    <?php pll_e('To nie przelewki. Przed nami 5-ta edycja bitwy. Szykujcie się do starcia!') ?>
                </p>
                <p>
                    <?php pll_e('O co chodzi?') ?>
                    <br>
                    <?php pll_e('Raz do roku spotykamy się, aby udowodnić, kto tak naprawdę rządzi na parkiecie. Podzieleni na dwie grupy (Ladies i Gentlemen) pod przywództwem doświadczonych w boju tancerzy przygotowujemy choreografie, które mają na celu znokautowanie przeciwnika. Pełne ekspresji ruchy, rytmiczna ofensywa, szturm za pomocą ciekawych formacji, rozpraszanie czy ośmieszanie wroga - wszystkie chwyty dozwolone!') ?>
                </p>
                <p>
                    <?php pll_e('Do tej pory nie wyłanialiśmy zwycięzcy - i tu pierwsza nowina: tym razem mówimy stanowcze NIE remisowi. Niech wygra lepszy!') ?>
                </p>

                <?php pll_e('Czekają na nas:') ?>
                <ul>
                    <li><?php pll_e('przygotowania do bitwy z tancerzami znanymi z rywalizacji na międzynarodowych festiwalach,')?></li>
                    <li><?php pll_e('2 poziomy zaawansowania w każdej ekipie,')?></li>
                    <li><?php pll_e('co najmniej 2 imprezy z dobrą muzyką,')?></li>
                    <li><?php pll_e('niedzielne zajęcia z cyklu “bajeranckie rzeczy, które można zrobić w Lindy Hopie” dla leaderów i followerek,')?></li>
                    <li><?php pll_e('konkursy i występy,')?></li>
                    <li><?php pll_e('rozstrzygnięcie odwiecznej wojny płci: bitwa Ladies vs Gentlemen Battle!')?></li>
                    <li><?php pll_e('i… hola, hola, nie zdradzimy wszystkiego tak od razu. ;)')?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- section end -->