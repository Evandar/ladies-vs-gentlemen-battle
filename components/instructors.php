<!-- program start -->
<!-- ================ -->


<section id="teachers" class="light-gray-bg pv-30 clearfix">
    <div class="main col-12">

        <!-- page-title start -->
        <!-- ================ -->
        <h2 class="mt-4 text-center"><?php pll_e('Instruktorzy'); ?></h2>
        <div class="separator"></div>
    </div>
    <div class="row grid-space-0 ">
        <div class="col-md-4 offset-md-1">
            <div class="team-member image-box style-2 dark-bg text-center mb-20 mt-20">
                <img src="<?php echo get_template_directory_uri(); ?>/images/vassia.jpg" alt="">
                <div class="body">
                    <h3 class="margin-clear">Vassia Panagiotou</h3>
                    <small>Ladies Team</small>
                    <div class="separator mt-10"></div>
                    <p class="small"><?php pll_e('Vassia Panagiotou jest miłośniczką klasycznego jazzu. Tańczy Lindy Hop i Authentic Jazz. Uwielbia improwizować i bawić się rytmem odtwarzając kroki ze starych filmów.'); ?></p>
                    <p class="small"><?php pll_e('Prowadzi grupę Rhythm Hoppers Chorus Line. Jest również organizatorką Athens Rhythm Hop (międzynarodowy festiwal swingowy) i współzałożycielką Rhythm Hoppers – szkoły tańca trudniącej się promowaniem Lindy Hopa i swingowej kultury.'); ?></p>
                    <p class="small"><?php pll_e('Poza rodzinną Grecją, znajdziecie ją na imprezach na całym świecie, gdzie uczy, tańczy i poszukuje nowych źródeł inspiracji. Często występuje w konkursach i ma na koncie wiele zwycięstw i tytułów..'); ?></p>
                    <p class="small"><?php pll_e('Poza Lindy Hopem, interesuje się także tańcem afrykańskim i stepowaniem.'); ?></p>
                    <div class="separator mt-10"></div>

                </div>
            </div>
        </div>
        <div class="col-md-2 vs-separator">
            <p class="vs">
                VS
            </p>
        </div>
        <div class="col-md-4">
            <div class="team-member image-box style-2 dark-bg text-center mb-20 mt-20">
                <img src="<?php echo get_template_directory_uri(); ?>/images/alexey.jpg" alt="">
                <div class="body">
                    <h3 class="margin-clear">Alexey Kazennov</h3>
                    <small>Gentlemen Team</small>
                    <div class="separator mt-10"></div>
                    <p class="small">
                        <?php pll_e('Alexey jest niezwykle utalentowanym i inspirującym tancerzem pochodzącym z Rosji.');
                        ?>
                    </p>
                    <p class="small">
                        <?php pll_e('Tańczyć zaczął w 2013; pojawiał się na wszystkich możliwych imprezach i jeszcze w tym samym roku dostał się do finałów J&J dla początkujących. W 2015 wpadł po uszy w wir Lindy Hopowego życia – zaczął uczyć, ćwiczyć i podróżować po całym świecie wygrywając niezliczone konkursy. Lindy hop, Authentic Jazz, Blues, St. Louis Shag – żaden taniec mu niestraszny! Bazując na tradycji i ciągłym poznawaniu korzeni swinga wychodzi ze świeżymi pomysłami, gdzie stawia na zabawę i klarowność.');
                        ?>
                    </p>
                    <p class="small">
                        <?php pll_e('Alexey jest znany ze swojego stylu, szerokiego uśmiechu i przede wszystkim z energii na parkiecie! Dacie wiarę, że ktoś dałby radę tańczyć 24 godziny non-stop? Tak, to właśnie Alexey!');
                        ?>
                    </p>
                    <div class="separator mt-10"></div>
                </div>
            </div>
        </div>

    </div>
</section>
