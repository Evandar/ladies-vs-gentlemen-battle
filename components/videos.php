<!-- section start -->
<!-- ================ -->
<section id="videos" class="dark-bg pv-30 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="text-center"><?php echo __('Filmiki'); ?></h2>
                <div class="separator"></div>

                <div id="yearGroup">
                    <ul class="year-list">
                        <li data-toggle="collapse" href="#video2015" role="button" aria-expanded="false" data-parent="#yearGroup">
                            <span>2015</span>
                        </li>

                        <li data-toggle="collapse" href="#video2016" role="button" aria-expanded="false" data-parent="#yearGroup">
                            <span>2016</span>
                        </li>

                        <li data-toggle="collapse" href="#video2017" role="button" aria-expanded="false" data-parent="#yearGroup">
                            <span>2017</span>
                        </li>

                        <li data-toggle="collapse" href="#video2018" role="button" aria-expanded="false" data-parent="#yearGroup">
                            <span>2018</span>
                        </li>

                        <li data-toggle="collapse" href="#video2019" role="button" aria-expanded="false" data-parent="#yearGroup">
                            <span>2019</span>
                        </li>

                    </ul>
                    <div class="collapse" id="video2015">
                        <div class="text-center">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/uLbM15hXaqQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="collapse" id="video2016">
                        <div class="text-center">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/zdSNzC7bglI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="collapse" id="video2017">
                        <div class="text-center">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/Qkp0vKA7n4M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="collapse" id="video2018">
                        <div class="text-center">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/CtTzsKQ3YHI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="collapse" id="video2019">
                        <div>
                            <img class="m-auto" src="https://media1.tenor.com/images/5772c0dd36dd7440c449afc8dd50d913/tenor.gif?itemid=4791402" width="400" height="400" alt="Waiting Cat GIF - Waiting Cat Filing GIFs" style="max-width: 833px; background-color: rgb(102, 93, 87);">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section><!-- section end -->

<script>
    jQuery('.year-list li').click( function(e) {
        jQuery('.collapse').collapse('hide');
    });
</script>