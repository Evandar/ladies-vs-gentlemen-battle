<!-- Price start -->
<!-- ================ -->
<section id="prices" class="section dark-translucent-bg tickets-bg background-img-1">
    <div class="container">
        <div class="row grid-space-10">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h2 class="mt-4 text-center"><?php pll_e('Bilety'); ?></h2>
                <div class="separator"></div>
                <!-- page-title end -->

                <!-- pricing tables start -->
                <!-- ================ -->
                <div class="pricing-tables object-non-visible" data-animation-effect="fadeInUpSmall"  data-effect-delay="0">
                    <div class="row">
                        <!-- pricing table start -->
                        <!-- ================ -->
                        <div class="col-md-4 plan">
                            <div class="header dark-bg">
                                <h3><?php pll_e('Battle Pass'); ?></h3>
                                <div class="price">
                                    <div class="price-small" ><?php pll_e('do 05.02'); ?></div>

                                    <div class="price-table">
                                        <div class="price-table-tr price-small">
                                            <div><?php pll_e('Tancerze SRT*'); ?></div>
                                            <div ><?php pll_e('Pozostali'); ?></div>
                                        </div>
                                        <div class="price-table-tr">
                                            <div><strong>180zł</strong></div>
                                            <div><strong>200 zł</strong></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="price">
                                    <div class="price-small" ><?php pll_e('od 06.02'); ?></div>

                                    <div class="price-table">
                                        <div class="price-table-tr price-small">
                                            <div><?php pll_e('Tancerze SRT*'); ?></div>
                                            <div ><?php pll_e('Pozostali'); ?></div>
                                        </div>
                                        <div class="price-table-tr">
                                            <div><strong>200 zł</strong></div>
                                            <div><strong>220 zł</strong></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="shadow light-gray-bg bordered">
                                <li>
                                    <?php pll_e('5h zajęć Solo Jazz '); ?>
                                    <br>
                                    <?php pll_e('(przygotowanie do bitwy)'); ?>
                                </li>
                                <li><?php pll_e('Impreza Ladies vs Gentlemen Battle'); ?></li>
                            </ul>
                        </div>
                        <!-- pricing table end -->

                        <!-- pricing table start -->
                        <!-- ================ -->
                        <div class="col-md-4 plan best-value">
                            <div class="header default-bg">
                                <h3><?php pll_e('Full Pass'); ?></h3>
                                <div class="price">
                                    <div class="price-small" ><?php pll_e('do 05.02'); ?></div>

                                    <div class="price-table">
                                        <div class="price-table-tr price-small">
                                            <div><?php pll_e('Tancerze SRT*'); ?></div>
                                            <div ><?php pll_e('Pozostali'); ?></div>
                                        </div>
                                        <div class="price-table-tr">
                                            <div><strong>230 zł</strong></div>
                                            <div><strong>250 zł</strong></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="price">
                                    <div class="price-small" ><?php pll_e('od 06.02'); ?></div>

                                    <div class="price-table">
                                        <div class="price-table-tr price-small">
                                            <div><?php pll_e('Tancerze SRT*'); ?></div>
                                            <div ><?php pll_e('Pozostali'); ?></div>
                                        </div>
                                        <div class="price-table-tr">
                                            <div><strong>250 zł</strong></div>
                                            <div><strong>270 zł</strong></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="shadow light-gray-bg bordered">
                                <li>
                                    <?php pll_e('5h zajęć Solo Jazz '); ?>
                                    <br>
                                    <?php pll_e('(przygotowanie do bitwy)'); ?>
                                </li>
                                <li>
                                    <?php pll_e('Practice z Alexeyem i Vassią') ?>
                                </li>
                                <li>
                                    <?php pll_e("2h zajęć Leaders' lub Followers' track") ?>
                                </li>
                                <li>
                                    <?php pll_e('Impreza Ladies vs Gentlemen Battle') ?>
                                </li>
                            </ul>
                        </div>
                        <!-- pricing table end -->

                        <!-- pricing table start -->
                        <!-- ================ -->
                        <div class="col-md-4 plan">
                            <div class="header dark-bg">
                                <h3><?php pll_e('Sunday Pass'); ?></h3>
                                <div class="price">
                                    <div class="price-small" ><?php pll_e('do 05.02'); ?></div>

                                    <div class="price-table">
                                        <div class="price-table-tr price-small">
                                            <div><?php pll_e('Tancerze SRT*'); ?></div>
                                            <div ><?php pll_e('Pozostali'); ?></div>
                                        </div>
                                        <div class="price-table-tr">
                                            <div><strong>100 zł</strong></div>
                                            <div><strong>110 zł</strong></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="price">
                                    <div class="price-small" ><?php pll_e('od 06.02'); ?></div>

                                    <div class="price-table">
                                        <div class="price-table-tr price-small">
                                            <div><?php pll_e('Tancerze SRT*'); ?></div>
                                            <div ><?php pll_e('Pozostali'); ?></div>
                                        </div>
                                        <div class="price-table-tr">
                                            <div><strong>110 zł</strong></div>
                                            <div><strong>120 zł</strong></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="shadow light-gray-bg bordered">
                                <li>
                                    <?php pll_e("2h zajęć Leaders' lub Followers' track") ?>
                                </li>
                                <li>
                                    <?php pll_e('Practice z Alexeyem i Vassią') ?>
                                </li>
                                <li>
                                    <?php pll_e('Impreza Ladies vs Gentlemen Battle') ?>
                                </li>
                            </ul>
                        </div>
                        <!-- pricing table end -->
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <?php pll_e('* Tancerze SRT - Uczestnicy zajęć regularnych Swing Revolution Trójmiasto z wykupionym karnetem na styczeń/luty'); ?>
                </div>
                <!-- pricing tables end -->
            </div>
            <!-- main end -->
        </div>
        <div class="col-lg-4 offset-lg-4 text-center">


            <br>
        </div>
    </div>
</section>
<!-- section end -->