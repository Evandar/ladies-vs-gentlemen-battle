<head>
    <meta charset="utf-8">
    <title>Ladies vs Gentlemen Battle</title>
    <meta name="description" content="The only one occasion to chose the winner!">
    <meta property="og:url"                content="https://ladiesvsgentlemenbattle.pl" />
    <meta property="og:title"              content="Ladies vs Gentlemen Battle" />
    <meta property="og:description"        content="The only one occasion to choose the real winner!" />
    <meta property="og:image"              content="http://ladiesvsgentlemenbattle.pl/wp-content/themes/ladies-vs-gentlemen-battle/images/fbheader.jpg" />
    <meta name="author" content="Tomasz Miotk">
    <meta name="theme-color" content="#505050" />
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <!-- Web Fonts -->
    <link href='//fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald" /><link rel='stylesheet' href='//fonts.googleapis.com/css?family=Oswald' type='text/css' />
    <?php wp_head(); ?>

</head>