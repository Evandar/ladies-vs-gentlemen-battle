<!-- section start -->
<!-- ================ -->
<section id="registration" class="light-gray-bg pv-30 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="text-center"><?php echo __('Rejestracja'); ?></h2>
                <div class="separator"></div>
                <?php
                $deadline = strtotime("2019-01-11 15:00:00");
                $now = time();
                $show = ($deadline - $now);
                ?>
                <?php if ($show <= 0): ?>
                    <?php
                    echo '<div>';
                    dynamic_sidebar("registration_form");
                    echo '</div>';
                    ?>
                <?php else: ?>
                    <p class="text-center"> <?php pll_e('Rejestracja wystartuje 11.01.2019r. o godzinie 16.00'); ?></p>
                <?php endif ?>
            </div>
        </div>
    </div>
</section>
<!-- section end -->