<?php
    class Microdot_Walker_Nav_Menu extends Walker_Nav_Menu {
        public function start_lvl( &$output, $depth = 0, $args = array() ) {
            $output .= '<ul>';
        }

        public function end_lvl( &$output, $depth = 0, $args = array() ) {
            $output .= '</ul>';
        }

        public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            $output .= $indent . '<li class="nav-item">';

            $item_output = '';
            $url = '';

            if( !empty( $item->url ) ) {
                $url = $item->url;
            }
            $item_output .= '<a class="nav-link" href="' . $url . '">';

            $item_output .= pll__(apply_filters( 'the_title', $item->title, $item->ID )) ;
            $item_output .= ( $args->walker->has_children ) ? '&nbsp;&nbsp;<i class="fa fa-caret-down"></i></a>' : '</a>';

            $item_output .= $args->after;

            $output .= $item_output;
        }

        public function end_el( &$output, $item, $depth = 0, $args = array() ) {
            $output .= '</li>';
        }
}


