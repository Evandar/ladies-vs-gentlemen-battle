<?php /* Template Name: ArchivePage */ ?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="zxx" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="zxx" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="zxx">
<!--<![endif]-->

<?php include 'components/head.php'; ?>

<body data-spy="scroll" data-target=".navbar" data-offset="50" class="transparent-header gradient-background-header">
<!-- scrollToTop -->
<!-- ================ -->
<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>

<!-- page wrapper start -->
<!-- ================ -->
<div class="page-wrapper">
    <?php include 'components/header.php'; ?>

    <?php include 'components/slider.php'; ?>

    <?php include 'components/all_news.php'; ?>

    <?php include 'components/footer.php'; ?>

</div>
<!-- page-wrapper end -->

<?php wp_footer(); ?>
<?php include 'components/foot.php'; ?>

</body>
</html>

